var getMissingChars = require('./codeChallenge.js');
var should = require('should');

/**
*A collection of helper functions to help generate strings of different character
*ranges or sets that will be useful in the tests
**/
function getLongInput(length){
	var i;
	var longInput = '';
	for(i = 0; i<length; i+=1){
		longInput+=getAllCharsNotIn('');
	}
	return longInput;
}

function getAllCharsInRange(start, end, selector){
	selector = selector ? selector : function(i){return String.fromCharCode(i);} ;
	var i;
	var chars = '';
	for(i = start.charCodeAt(0); i<=end.charCodeAt(0); i+=1){
		chars += selector(i);
	}
	return chars;
};

function getEveryEvenChar(start,end){
	return getAllCharsInRange(start,end,function(i){
		return i%2==0 ? String.fromCharCode(i) : '';
	});
}

function getAllCharsNotIn(set){
	var allChars = getAllCharsInRange('\0','\xFF');
	var notAllChars = allChars.split('').filter(function(x){return set.indexOf(x)==-1;}).join();
	return notAllChars;
}

function getAllCharsNotInRange(start,end){
	return getAllCharsNotIn(getAllCharsInRange(start,end));
}

function getEveryOddChar(start,end){
	return getAllCharsInRange(start,end,function(i){
		return i%2!=0 ? String.fromCharCode(i) : '';
	});
}

function checkMissingChars(input, expected){
	var actual = getMissingChars(input);
	actual.should.be.exactly(expected);
}

function checkPangram(input){
	checkMissingChars(input,success);
}

var success = 'This is a Pangram';
var allCharFailure = getAllCharsInRange('a','z');

describe('#getMissingChars',function(){
	describe('Generic failure cases',function(){
		it('should return all chars for null',function(){
			checkMissingChars(null, allCharFailure)
		});
		it('should return all chars for an empty string',function(){
			checkMissingChars('',allCharFailure);
		});
		it('should return all chars for a string that has every char except alphabetic ones',function(){
			checkMissingChars(getAllCharsNotInRange('A','z')+getAllCharsInRange('[','`'),allCharFailure);
		});
		it('should return every other char for a string that is missing every other',function(){
			checkMissingChars(getEveryEvenChar('A','Z'),getEveryOddChar('a','z'));
		});
		it('should return the chars not in a custom test string',function(){
			checkMissingChars('!@#$%^&*(()josh was here AZDEFG 103929840192384091283`````~~~','bciklmnpqtuvxy');
		});
		it('should return all characters for a whitespace string',function(){
			checkMissingChars('\t\t\t\t\t\t\t                        \n\n',allCharFailure);
		});
		
	});
	describe('Generic success cases', function(){
		it('should pass for the classic all letter sentence',function(){
			checkPangram('The quick brown fox jumps over a lazy dog');
		});
		it('should pass for a custom set of chars',function(){
			checkPangram('This!@# iS a SentenCE that WILL 3ventually pass the 8100913204 full character count probably\
						 somehow agg18910~-0-87/*/*THERE ARE JUST EX"""\nACTLY A LOT OF werDIFFERENT WAYS OF not passing all\
						 the characters like z is pretty hard to get along with qwrqe \t \0xFF Q');
		});
		it('should pass for all of the chars in the normal ASCII set',function(){
			checkPangram(getAllCharsNotIn(''));
		});
		it('should pass for the lowercase range of chars',function(){
			checkPangram(getAllCharsInRange('a','z'));
		});
		it('should pass for the uppercase range of chars', function(){
			checkPangram(getAllCharsInRange('A','Z'));
		});
		it('should pass for 50/50 upper/lower case chars of the full alphabet',function(){
			checkPangram(getEveryEvenChar('A','Z')+getEveryOddChar('a','z'));
		});
		it('should pass for a really long string of chars', function(){
			checkPangram(getLongInput(500));
		});		
	});
});
