# README #
Answers to the QA Assessment can be found [here](https://docs.google.com/document/d/1IR1cC4ye3pVPfOodFD_8MTA5ugOoruP43Tit3BHhCzY/edit?usp=sharing)

This repo holds code and tests written to satisfy the challenge below.

Code Challenge:

Write a function or program that takes as its input a string and prints “This is a Pangram” value if the string is a pangram (a sequence of letters containing at least one of each letter in the English alphabet) and list of missing character value otherwise.

Case of letters should be ignored; If the string is abcdefghijklmnopqrstuvwXYZ, then the function should still return “This is Pangram”. Note that the string can contain any other characters in it, so 123abcdefghijklm NOPQRSTUVWXYZ321 would return a “This is a Pangram”. An empty input should return all the missing characters

Also, if time permits I would like to see unit test around this code.

Unit test should cover positive as well as negative test cases.


### How do I get set up? ###


1. Run `npm install` to get all of the required modules
2. Run `npm test` to see all unit tests passing