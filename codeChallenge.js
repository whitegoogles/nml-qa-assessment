

//Should run in O(n) time, which is the lowest possible bound for a problem
//like this. Could be written with a regex probably but wouldn't achieve
//the same bound.
function getMissingChars(input){
	
	//Store all of the characters found in the alphanumeric range in a set.
	var flags = {};
	
	input = (input == null) ? "" : ""+input;
	var i;
	for(i = 0; i<input.length; i+=1){
		var curChar = input[i].toLowerCase();	
		flags[curChar] = 1;
	}
	
	//Records all of the missing characters based upon the flags set
	var missingChars = "";
	for(i = "a".charCodeAt(0); i<="z".charCodeAt(0); i+=1){
		var checkChar = String.fromCharCode(i);
		missingChars+=flags[checkChar] ? "":checkChar;
	}
	return missingChars === "" ? "This is a Pangram": missingChars;
}

module.exports = getMissingChars;